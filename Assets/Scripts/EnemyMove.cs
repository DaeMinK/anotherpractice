﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour
{

    Rigidbody2D rigid;
    Animator anim;
    SpriteRenderer spriteRenderer;
    CapsuleCollider2D capsuleCollider;


    public int nextMove;
    
    void Awake()
    {
        rigid = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        capsuleCollider = GetComponent<CapsuleCollider2D>();

        //5초동안 딜레이 됨.
        Invoke("Think", 5);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rigid.velocity = new Vector2(nextMove, rigid.velocity.y);

        //적의 낙하방지( 지형체크 )
        Vector2 frontvec = new Vector2(rigid.position.x + nextMove *0.2f, rigid.position.y);
        Debug.DrawRay(frontvec, Vector3.down, new Color(0, 1, 0));
        RaycastHit2D rayHit = Physics2D.Raycast(frontvec, Vector3.down, 1, LayerMask.GetMask("Platform"));
        if (rayHit.collider == null)
        {
            Turn();
        }
    }

    void Think()
    {
        //다음 활동
        nextMove = Random.Range(-1, 2);

        anim.SetInteger("WalkSpeed", nextMove);

        //0이 아니고 1이면 왼쪽 아니면 오른쪽을 본다 
        if (nextMove != 0)
        {
            spriteRenderer.flipX = nextMove == 1;
        }

        //더한 랜덤성 무빙
        float nextThinkTime = Random.Range(2f, 5f);

        //재귀함수     딜레이 없이 사용하면 매우 위험!  맨 아래에 넣는게 제일 안전하다.
        Invoke("Think", nextThinkTime);


    }

    void Turn()
    {
        nextMove = nextMove * -1;
        spriteRenderer.flipX = nextMove == 1;
        //인보크 초기화
        CancelInvoke();
        Invoke("Think", 2);
    }

    public void OnDamaged()
    {
        //sprite alpha
        spriteRenderer.color = new Color(1, 1, 1, 0.4f);
        //Sprite Flip Y
        spriteRenderer.flipY = true;
        //collider disable
        capsuleCollider.enabled = false;
        //Die Effect 
        rigid.AddForce(Vector2.up * 5, ForceMode2D.Impulse);
        //Destroy
        Invoke("DeActive", 5);
    }

    void DeActive()
    {
        gameObject.SetActive(false);
    }

}
