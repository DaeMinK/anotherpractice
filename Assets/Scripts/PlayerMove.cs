﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public GameManager gameManager;
    public AudioClip audioJump;
    public AudioClip audioAttack;
    public AudioClip audioDamaged;
    public AudioClip audioItem;
    public AudioClip audioDie;
    public AudioClip audioFinish;

    public float maxSpeed;
    public float jumpPower;
    Rigidbody2D rigid;
    SpriteRenderer spriteRenderer;
    Animator anim;
    CapsuleCollider2D capsule_pl;
    AudioSource audioSource;

    void Awake()
    {
        rigid = GetComponent<Rigidbody2D>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        capsule_pl = GetComponent<CapsuleCollider2D>();
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {

        //jump
        if (Input.GetButtonDown("Jump") && !anim.GetBool("isJumping")) { 
            rigid.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
            anim.SetBool("isJumping", true);
            PlaySound("JUMP");
            audioSource.Play();
        }



        //stop speed
        if(Input.GetButtonUp("Horizontal"))
        {
            //normalized : 벡터 크기를 1로 만든 상태     
            rigid.velocity = new Vector2(rigid.velocity.normalized.x * 0.5f, rigid.velocity.y);
        }
        if(Input.GetButton("Horizontal"))
            spriteRenderer.flipX = Input.GetAxisRaw("Horizontal") == -1;

        //걷기 Mathf = 기본제공 함수 , Abs = 절대값
        if (Mathf.Abs(rigid.velocity.x) < 0.3)
            anim.SetBool("isWalking", false);
        //멈추기
        else 
            anim.SetBool("isWalking", true);

    }

    void FixedUpdate()
    {//FixedUpdate는 단발적인용도로?
        //좌우입력        
        float h = Input.GetAxisRaw("Horizontal");

        rigid.AddForce(Vector2.right * h*2, ForceMode2D.Impulse);

        //velocity 리지드바디의 현재 속도
        //최고 속도 못넘게 하기.
        if (rigid.velocity.x > maxSpeed)
            rigid.velocity = new Vector2(maxSpeed, rigid.velocity.y);
        //최저속도 관리
        else if (rigid.velocity.x < maxSpeed * (-1))
            rigid.velocity = new Vector2(maxSpeed * (-1), rigid.velocity.y);

        //RayCast = 오브젝트 검색을 위해 ray를 쏘는 방식
        //바닥에 닿는지 확인
        //DrawRay = 에디터 상에서만 ray를 그려줌
        if (rigid.velocity.y < 0) //y값이 0보다 작아지면 발생
        {
            Debug.DrawRay(rigid.position, Vector3.down, new Color(0, 1, 0));
            //RayCastHit = ray에 닿은 오브젝트
            //GetMask() = 레이어 이름에 해당하는 정수값을 리턴하는 함수
            RaycastHit2D rayHit = Physics2D.Raycast(rigid.position, Vector3.down, 1, LayerMask.GetMask("Platform"));
            if (rayHit.collider != null)
            {
                if (rayHit.distance < 0.5f)
                    anim.SetBool("isJumping", false);
                //Debug.Log(rayHit.collider.name);
            }
        }
        /*      
              이 방식으로하면 유저만 레이히트 당함.    
              레이히트가 되지 않았으면, 그리고 레이히트는 관통이 안된다
              if (rayHit.collider != null)
              {
                  //print(rayHit.collider.name); print 표현법 
                  Debug.Log(rayHit.collider.name); // 디버그로그 표현법
              }
        */
    }
        
    //충돌!
    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Enemy")
        {   
            //밟기
            if(rigid.velocity.y < 0 && transform.position.y > collision.transform.position.y)
            {
                OnAttack(collision.transform);
            }
            else // 아니면 쳐맞는거여 아주그냥
            OnDamaged(collision.transform.position);
        }        


    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Item")
        {
            //point
            bool isBronze = collision.gameObject.name.Contains("Bronze");
            bool isSilver = collision.gameObject.name.Contains("Silver");
            bool isGold = collision.gameObject.name.Contains("Gold");

            if (isBronze)
                gameManager.stagePoint += 50;
            else if (isSilver)
                gameManager.stagePoint += 100;
            else if (isGold)
                gameManager.stagePoint += 300;


            //사라짐
            collision.gameObject.SetActive(false);

            PlaySound("ITEM");
            audioSource.Play();
        }
        else if (collision.gameObject.tag == "Finish")
        {
            //Next Stage
            gameManager.NextStage();
            PlaySound("FINISH");
            audioSource.Play();
        }
    }

    void OnAttack(Transform enemy)
    {
        //점수
        gameManager.stagePoint += 100;
        //반작용
        rigid.AddForce(Vector2.up * 5, ForceMode2D.Impulse);

        // 적 사망
        EnemyMove enemyMove = enemy.GetComponent<EnemyMove>();
        enemyMove.OnDamaged();
        PlaySound("ATTACK");
        audioSource.Play();

    }

    //기능이 있는건 따로 만드는게 좋다.
    void OnDamaged(Vector2 targetPos)
    {
        //health down
        gameManager.HealthDown();
        
        //레이어 번호!
        gameObject.layer = 11;

        //쳐맞을떄 플레이어 색깔
        spriteRenderer.color = new Color(1, 1, 1, 0.4f);

        // 쳐맞는 효과
        int dirc = transform.position.x - targetPos.x > 0 ? 1 : -1;
        rigid.AddForce(new Vector2(dirc, 1)*6, ForceMode2D.Impulse);

        //anim
        anim.SetTrigger("doDamaged");

        PlaySound("DAMAGED");
        audioSource.Play();

        Invoke("OffDamaged", 1);

    }

    void OffDamaged()
    {
        gameObject.layer = 10;
        spriteRenderer.color = new Color(1, 1, 1, 1);
    }

    public void OnDie()
    {
        spriteRenderer.color = new Color(1, 1, 1, 0.4f);
        //Sprite Flip Y
        spriteRenderer.flipY = true;
        //collider disable
        capsule_pl.enabled = false;
        //Die Effect 
        rigid.AddForce(Vector2.up * 5, ForceMode2D.Impulse);

        PlaySound("DIE");
        audioSource.Play();
    }

    public void VelocityZero()
    {
        rigid.velocity = Vector2.zero;

    }

    void PlaySound(string action)
    {
        switch (action)
        {
            case "JUMP":
                audioSource.clip = audioJump;
                break;
            case "ATTACK":
                audioSource.clip = audioAttack;
                break;
            case "DAMAGED":
                audioSource.clip = audioDamaged;
                break;
            case "ITEM":
                audioSource.clip = audioItem;
                break;
            case "DIE":
                audioSource.clip = audioDie;
                break;
            case "FINISH":
                audioSource.clip = audioFinish;
                break;
        }
    }

}
